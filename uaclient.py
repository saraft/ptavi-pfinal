#!/usr/bin/python3
# -*- coding: utf-8 -*-

import secrets
import socket
import sys
from xml.dom import minidom
from xml.sax.handler import ContentHandler
from xml.sax import make_parser
import time
import random
import simplertp


class SmallSMILHandler(ContentHandler):
    # así leo el fichero xml

    def __init__(self):
        super().__init__()
        self.etiq = {}
        self.dicc = {
            "account": ["username", "passwd"],
            "uaserver": ["ip", "puerto"],
            "rtpaudio": ["puerto"],
            "regproxy": ["ip", "puerto"],
            "log": ["path"],
            "audio": ["path"]
        }

    def startElement(self, name, attrs):

        if name in self.dicc:
            for atributos in self.dicc[name]:
                atributo = attrs.get(atributos, "")
                self.etiq[name + '-' + atributos] = atributo

    def get_etiq(self):
        return self.etiq


class createLog:
    global time_now

    def __init__(self):
        super().__init__()
        self.log = PATH_log

    def writeLog(self, data):
        with open(self.log, 'a') as fichLog:
            fichLog.write(data)

    def estado(self, estado):
        if estado == 'inicio':
            info = time_now + ' -Starting... ' + '\r\n'
            self.writeLog(info)
        elif estado == 'fin':
            mensaje = time_now + ' -Finishing. ' + '\r\n'
            self.writeLog(mensaje)

    def sendLog(self, IP, PORT, info):
        inff = info.replace('\r\n', '')
        info = time_now + ' -Send to: ' + IP + ': '
        info += str(PORT) + inff + '\r\n'
        self.writeLog(info)

    def recivedLog(self, IP, PORT, info):
        inff = info.replace('\r\n', '')
        info = time_now + ' -Recived from: ' + IP + ': '
        info += str(PORT) + inff + '\r\n'
        self.writeLog(info)

    def errorLog(self):
        info = time_now + ' -20101018160243 Error: No server listening at '
        info += IP_PR + 'port ' + str(PORT_PR) + '\r\n'
        self.writeLog(info)

    def rtpLog(self, IP, PORT, audio):
        info = time_now + ' -Sending to... ' + IP + PORT + audio + 'byRTP\r\n'
        self.writeLog(info)


if __name__ == "__main__":

    if len(sys.argv) != 4:
        sys.exit('Usage: python uaclient.py config method option')

    CONFIG = sys.argv[1]
    METODO = sys.argv[2]
    EXPIRES = sys.argv[3]
    time_now = time.strftime('%Y%m%d%H:%M:%S', time.gmtime(time.time()))

    # Manejamos el fichero de configuración
    parser = make_parser()
    parser.setContentHandler(SmallSMILHandler())
    parser.parse(open(CONFIG))
    info = SmallSMILHandler().get_etiq()

    # Así cojo las variables del xml
    mydoc = minidom.parse(CONFIG)
    account = mydoc.getElementsByTagName('account')
    username = account[0].attributes['username'].value

    contra = mydoc.getElementsByTagName('account')
    passwd = contra[0].attributes['passwd'].value

    rtpaudio = mydoc.getElementsByTagName('rtpaudio')
    PORT_RTP = rtpaudio[0].attributes['puerto'].value

    ipserv = mydoc.getElementsByTagName('uaserver')
    IP_S = ipserv[0].attributes['ip'].value

    portserv = mydoc.getElementsByTagName('uaserver')
    PORT_S = portserv[0].attributes['puerto'].value

    ipprox = mydoc.getElementsByTagName('regproxy')
    IP_PR = ipprox[0].attributes['ip'].value

    portpr = mydoc.getElementsByTagName('regproxy')
    PORT_PR = portpr[0].attributes['puerto'].value

    pathlog = mydoc.getElementsByTagName('log')
    PATH_log = pathlog[0].attributes['path'].value

    pathaudio = mydoc.getElementsByTagName('audio')
    PATH_Audio = pathaudio[0].attributes['path'].value

    log = createLog()
    estado = 'inicio'
    log.estado(estado)

    # Contenido que vamos a enviar
    # Creamos el socket, lo configuramos y lo atamos al servidor/puerto
    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
        try:
            my_socket.connect((IP_PR, int(PORT_PR)))
            my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            if METODO == 'REGISTER':
                INFO = 'REGISTER ' + 'sip:' + username
                INFO += 'IP:' + IP_PR + 'puerto:' + PORT_S
                INFO += ' SIP/2.0\r\n' + 'Expires: ' + EXPIRES + ' '
                INFO += 'CONTRSEnA: ' + passwd

                print("enviando: ... ", INFO)

                my_socket.send(bytes(INFO, 'utf-8') + b'\r\n')
                log.sendLog(IP_PR, PORT_PR, INFO)
                respuestaServidor = my_socket.recv(1024).decode('utf-8')

                print('recibido...', respuestaServidor)
                log.recivedLog(IP_PR, PORT_PR, respuestaServidor)

            if METODO == 'INVITE':
                INFO = METODO + " sip:" + username + '\r\n' + ' SIP/2.0\r\n'
                INFO += "Content-Type: application/sdp\r\n"
                INFO += "Content-Length: 76\r\n\r\n"
                INFO += "v=0\r\n"
                INFO += "o=" + username + " " + IP_S
                INFO += "\r\ns=misesion\r\n"
                INFO += "t=0\r\n"
                INFO += "m=audio " + PORT_RTP + " RTP\r\n"
                print('enviando al proxy...:' + INFO)
                my_socket.send(bytes(INFO, 'utf-8') + b'\r\n')
                # INFO = INFO.replace("r\n", " ")
                log.sendLog(IP_PR, PORT_PR, INFO)

                data = my_socket.recv(1024).decode('utf-8')
                print('recibo...: ' + data)
                log.recivedLog(IP_PR, PORT_PR, INFO)

                my_socket.send(bytes(INFO, 'utf-8') + b'\r\n')
                log.sendLog(IP_PR, PORT_PR, INFO)

                # creo ack una vez que reciba  INVITE
                respuestaServidor = data.split()
                n1 = respuestaServidor[1]
                if n1 == '100':
                    METODO1 = 'ACK'
                    INFO = METODO1 + " sip:" + username + '\r\n' + ' SIP/2.0\r\n'
                    INFO += 'IP ' + IP_S + ' PuertoRTP: ' + PORT_RTP + '\r\n'
                    print('enviando al proxy...:' + INFO)
                    my_socket.send(bytes(INFO, 'utf-8') + b'\r\n')
                    data = my_socket.recv(1024).decode('utf-8')
                    log.sendLog(IP_PR, PORT_PR, INFO)

                    ALEAT = random.randint(1, 15)
                    BIT = secrets.randbits(1)
                    # PORT = data.split("PuertoRTP: ")[1]

                    csrc = []

                    RTP_header = simplertp.RtpHeader()
                    RTP_header.set_header(version=2, marker=BIT,
                                        payload_type=14,
                                        ssrc=ALEAT)
                    RTP_header.setCSRC(csrc)
                    audio = simplertp.RtpPayloadMp3(PATH_Audio)
                    simplertp.send_rtp_packet(RTP_header, audio, "127.0.0.1", 1234)

                else:
                    print('error')

            if METODO == 'BYE':
                INFO = METODO + " sip:" + username + '\r\n' + ' SIP/2.0\r\n'
                print('enviando al proxy...:' + INFO)
                my_socket.send(bytes(INFO, 'utf-8') + b'\r\n')
                log.sendLog(IP_PR, PORT_PR, INFO)
                data = my_socket.recv(1024).decode('utf-8')
                print('Enviado el BYE al proxy-registrar' + '\r\n' + data)
                # data = data.replace("\r\n", " ")
                log.recivedLog(IP_PR, PORT_PR, INFO)

        except ConnectionRefusedError:
            log.errorLog()
            sys.exit("20101018160243 Error: No server listening at " +
                     IP_PR + "port " + str(PORT_PR))

    print("Terminando socket...")
    print("Fin.")
    estado = 'fin'
    log.estado(estado)
