#!/usr/bin/python3
# -*- coding: utf-8 -*-

import secrets
import socketserver
import sys
from xml.dom import minidom
import simplertp
import random
# import time
from xml.sax import make_parser
from xml.sax.handler import ContentHandler


class SmallSMILHandler(ContentHandler):
    # leo el xml

    def __init__(self):

        super().__init__()
        self.etiq = {}
        self.dicc = {
             "account": ["username", "passwd"],
             "uaserver": ["ip", "puerto"],
             "rtpaudio": ["puerto"],
             "regproxy": ["ip", "puerto"],
             "log": ["path"],
             "audio": ["path"]
        }

    def startElement(self, name, attrs):
        """
        Método que se llama cuando se abre una etiqueta
        """
        if name in self.dicc:
            for atributos in self.dicc[name]:
                atributo = attrs.get(atributos, "")
                self.etiq[name + "_" + atributos] = atributo

    def get_etiq(self):
        return self.etiq


class EchoHandler(socketserver.DatagramRequestHandler):
    """
    Echo server class
    """
    diccRTP = {}

    def handle(self):
        # Escribe dirección y puerto del cliente (de tupla client_address)
        global info

        # Leyendo línea a línea lo que nos envía el cliente
        mensaje = self.rfile.read().decode('utf-8')
        print("El cliente: manda " + mensaje)

        if mensaje != '\r\n':
            datosmensaje = mensaje.split()
            # print(datosmensaje)
            METODO = datosmensaje[0]
            IP_C, PORT_C = self.client_address

            if METODO == 'INVITE':
                # conseguimos la ip del emisor
                INFO = "SIP/2.0 100 Trying" + "\r\n"
                INFO += "SIP/2.0 180 Ring" + "\r\n"
                INFO += "SIP/2.0 200 OK" + "\r\n"
                INFO += "Content-Type: application/sdp\r\n"
                INFO += "Content-Length: 76\r\n\r\n"
                self.wfile.write(bytes(INFO, 'utf-8'))
                info = datosmensaje

            elif METODO == 'BYE':
                INFO = b"SIP/2.0 200 OK" + b"\r\n\r\n"
                self.wfile.write(INFO)

            elif METODO == 'ACK':
                print('mensahehe', mensaje)
                ALEAT = random.randint(1, 15)
                BIT = secrets.randbits(1)
                csrc = []
                self.diccRTP['puertortp:'] = \
                    mensaje.split(' PuertoRTP: ')[1].split('\r\n')[0]
                PORT = self.diccRTP['puertortp:']

                RTP_header = simplertp.RtpHeader()
                RTP_header.set_header(version=2, marker=BIT,
                                      payload_type=14,
                                      ssrc=ALEAT)
                RTP_header.setCSRC(csrc)
                audio = simplertp.RtpPayloadMp3(PATH_Audio)
                simplertp.send_rtp_packet(RTP_header, audio, '127.0.0.1', PORT)

            elif METODO not in ['INVITE', 'ACK', 'BYE']:
                INFO = b"SIP/2.0 405 Method Not Allowed" + b"\r\n\r\n"
                self.wfile.write(INFO)

            else:
                INFO = b"SIP/2.0 400 Bad Request" + b"\r\n\r\n"
                self.wfile.write(INFO)


if __name__ == "__main__":
    if len(sys.argv) != 2:
        sys.exit('Usage: python uaserver.py config')

    parser = make_parser()
    parser.setContentHandler(SmallSMILHandler())
    parser.parse(open(sys.argv[1]))
    info = SmallSMILHandler().get_etiq()

    CONFIG = sys.argv[1]

    mydoc = minidom.parse(CONFIG)
    account = mydoc.getElementsByTagName('account')
    username = account[0].attributes['username'].value

    contra = mydoc.getElementsByTagName('account')
    passwd = contra[0].attributes['passwd'].value

    rtpaudio = mydoc.getElementsByTagName('rtpaudio')
    PORT_RTP = rtpaudio[0].attributes['puerto'].value

    ipserv = mydoc.getElementsByTagName('uaserver')
    IP_S = ipserv[0].attributes['ip'].value

    portserv = mydoc.getElementsByTagName('uaserver')
    PORT_S = portserv[0].attributes['puerto'].value

    ipprox = mydoc.getElementsByTagName('regproxy')
    IP_PR = ipprox[0].attributes['ip'].value

    portpr = mydoc.getElementsByTagName('regproxy')
    PORT_PR = portpr[0].attributes['puerto'].value

    pathlog = mydoc.getElementsByTagName('log')
    PATH_log = pathlog[0].attributes['path'].value

    pathaudio = mydoc.getElementsByTagName('audio')
    PATH_Audio = pathaudio[0].attributes['path'].value

    print('Escuchando...:')
    print("Lanzando el servidor UDP de eco...")
    serv = socketserver.UDPServer((IP_S, int(PORT_S)), EchoHandler)
    serv.serve_forever()

    try:
        print("Listening...")
        estado = 'inicio'
        serv.serve_forever()
    except KeyboardInterrupt:
        estado = 'fin'
        print("Finalizado")

    """
    esto no va
    # username = info["account_username"]
    #passwd = info["account_passwd"]
    #IP_S = info["uaserver-ip"]
    #PORT_S = info["uaserver-puerto"]
    #PORT_RTP = info["rtpaudio-puerto"]
    #IP_PR = info["regproxy-ip"]
    #PORT_PR = int(info["regproxy-puerto"])
    #PATH_log = info["log-path"]
    P#ATH_Audio = info["audio-path"]
    serv = socketserver.UDPServer((IP_S, int(PORT_S)), EchoHandler)
    serv.serve_forever()
    print('Escuchando...:')

    print("Lanzando el servidor UDP de eco...")
    """
