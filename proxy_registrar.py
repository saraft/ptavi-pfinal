#!/usr/bin/python3
# -*- coding: utf-8 -*-

import hashlib
import socket
import socketserver
import sys
from xml.dom import minidom
from xml.sax.handler import ContentHandler
import json
import time
from xml.sax import make_parser


class SmallSMILHandler(ContentHandler):
    # así leo el fichero xml

    def __init__(self):
        super().__init__()
        self.etiq = {}
        self.dicc = {
            "server": ["name", "ip", "puerto"],
            "database": ["path", "passwdpath"],
            "log": ["path"],
        }

    def startElement(self, name, attrs):
        """
        Método que se llama cuando se abre una etiqueta
        """
        if name in self.dicc:
            for atributos in self.dicc[name]:
                atributo = attrs.get(atributos, "")
                self.etiq[name + '-' + atributos] = atributo

    def get_etiq(self):
        return self.etiq


class SIPRegisterHandler(socketserver.DatagramRequestHandler):
    """ Para registrar y borrar del diccionario """
    dicc = {}
    dicc2 = {}

    # escribo en el fich json.
    def json2password(self):
        """escribo en un fichero json"""
        with open("passwords.json", "w") as fichjson:
            json.dump(self.dicc, fichjson)

    def json2registered(self):
        """si el fichero registered.json existe leo el contenido
        y uso el diccionario"""
        try:
            with open("passwords.json", "r+b") as fichjson:
                # print('sdfghgfxdfgfddfghgfxvbn')
                self.dicc = json.load(fichjson)
                # load para decodif lo del fich json
        except FileNotFoundError:
            pass
            print('paso de la vida y del fichero json')

    def registrarUsuario(self, username, IP_C, port, timeExpire):
        self.json2registered()
        self.dicc[username] = {'username': username,
                                'address': IP_C,
                                'puerto': port,
                                'expires': timeExpire}
        request = b'SIP/2.0 200 OK\r\n\r\n'
        self.wfile.write(request)

    def handle(self):
        """ Metodo Handle donde utilizo cada metodo """
        global datos
        global dirSIP

        mensaje = self.rfile.read().decode('utf-8')
        cabecera = ('Via: SIP/2.0/UDP ' + IP_PR + ' : ' + PORT_PR)
        cabecera += '\r\n'
        petition = mensaje + cabecera
        print("el cliente nos manda: " + petition)

        METODO = mensaje.split()[0]

        nowtime = time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime(time.time()))

        if METODO == 'REGISTER':
            self.json2registered()

            mydoc = minidom.parse(sys.argv[1])
            p = mydoc.getElementsByTagName('database')
            contrasena = p[0].attributes['passwdpath'].value

            Line = hashlib.md5()
            Line.update(bytes(str(self.dicc2), 'utf-8'))
            Line.update(bytes(contrasena, 'utf-8'))
            self.json2password()
            self.wfile.write(b"SIP/2.0 200 OK\r\n\r\n")

            print('La contra es: ' + mensaje[-7:])

            # declaramos variables y registramos usuario
            username = mensaje.split('sip:')[1].split('IP:')[0]
            IP_C = self.client_address[0]
            port = mensaje.split('puerto:')[1].split(' ')[0]
            timeExpire = mensaje.split('Expires: ')[1].split(' ')[0]
            tiempo = mensaje.split('Expires: ')[1].split(' ')[0]
            self.registrarUsuario(username, IP_C, port, timeExpire)
            print('El diccionario es: ' + str(self.dicc))

            # para borrar el dicionario si está caducado
            if mensaje.split()[3] == 'Expires:':
                print("va")
                if int(tiempo) == 0:
                    del self.dicc[username]
                    print("borramos diccionario")
            else:
                print('se ha guardado el registro')

        elif METODO == 'INVITE' or 'ACK' or 'BYE':
            self.json2registered()
            user = mensaje.split(' sip:')[1].split('\r\n')[0]

            if user in self.dicc:

                puerto = self.dicc[user]['puerto']
                ip = self.dicc[user]['address']
                # print('El usuario se registra
                # como: ' + ip + ' Puerto: ' + puerto)

                with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
                    # esto manda el invite al server
                    my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
                    my_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                    my_socket.connect((ip, int(puerto)))
                    print('Le reenviamos al server:')
                    # mensaje es INVITE sip:saraft

                    # print('esto es mensaje', mensaje)
                    my_socket.send(bytes(mensaje, 'utf-8') + b'\r\n\r\n')
                    DATA = my_socket.recv(1024).decode('utf-8')
                    print(DATA)
                    self.wfile.write(bytes(DATA, 'utf-8') + b'\r\n\r\n')

            else:
                    respuesta = ("SIP/2.0 404 User not found" + "\r\n\r\n")
                    self.wfile.write(bytes(respuesta, 'utf-8') + b'\r\n')

        elif METODO not in ['INVITE', 'ACK', 'BYE']:
                INFO = b"SIP/2.0 405 Method Not Allowed" + b"\r\n\r\n"
                self.wfile.write(INFO)
        else:
            INFO = b"SIP/2.0 400 Bad Request" + b"\r\n\r\n"
            self.wfile.write(INFO)

        self.json2password()


if __name__ == "__main__":
    if len(sys.argv) != 2:
        sys.exit('Usage: python uaserver.py config')

    parser = make_parser()
    cHandler = SmallSMILHandler()
    parser.setContentHandler(cHandler)
    parser.parse(open(sys.argv[1]))
    info = cHandler.get_etiq()

    # datos que cojo del Xml ua.xml
    username = info["server-name"]
    path = info["database-path"]
    passwd = info["database-passwdpath"]
    IP_PR = info["server-ip"]
    PORT_PR = info["server-puerto"]
    PATH_log = info["log-path"]

    print('Escuchando...:')
    serv = socketserver.UDPServer((IP_PR, int(PORT_PR)), SIPRegisterHandler)
    serv.serve_forever()

    try:
        estado = 'inicio'
        serv.serve_forever()
    except KeyboardInterrupt:
        estado = 'fin'
        print("Finalizado servidor")
